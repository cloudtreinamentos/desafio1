#!/usr/bin/env bash

echo "Start Build ...."
echo "... Building Docker Image ...."
docker build -f docker/Dockerfile --build-arg buildn="$BUILD_NUMBER" -t rfoot/desafio1:"$BUILD_NUMBER" .
docker images
echo "... Pushing Docker Image ...."
echo "$DPWD" | docker login -u "$DUSER" --password-stdin
docker push rfoot/desafio1:"$BUILD_NUMBER"
echo "... Removing Docker Image ...."
docker rmi rfoot/desafio1:"$BUILD_NUMBER"
echo "... Build Done!"
